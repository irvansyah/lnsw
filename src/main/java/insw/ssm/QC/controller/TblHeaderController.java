package insw.ssm.QC.controller;

import insw.ssm.QC.dto.TblHeaderLog;
import insw.ssm.QC.service.TblHeaderService;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;



import java.text.SimpleDateFormat;

@CrossOrigin
@RequestMapping("/v1/header")
@RestController
public class TblHeaderController extends BaseController {

        protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    TblHeaderService tblHeaderService;

    @GetMapping(value = "/getbytanggal", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation("/get")
    ResponseEntity findAll(TblHeaderLog request) {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.add("Content-Type", "application/vnd.openmlformats-officedocument.spreadsheetml.sheet");
            headers.add("Content-Disposition", "attachment; filename=" + request + ".xlsx");

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            sdf.setLenient(false);


            return new ResponseEntity<>(tblHeaderService.findAll(request), headers, HttpStatus.OK);

//            return ok(txSsmHeaderBpomService.findAll(request));
        } catch (Exception e) {
            logger.info(e.getMessage());
            return badRequest(e.getMessage());
        }

    }
    @GetMapping(value = "/getAll",produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation("ambil data")
    ResponseEntity getAll() {
        try {
            return ok(tblHeaderService.get());
        } catch (Exception e) {
            logger.debug(e.getMessage());
            return badRequest(e.getMessage());
        }
    }
}
