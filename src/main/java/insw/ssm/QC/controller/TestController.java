package insw.ssm.QC.controller;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
@RequestMapping("/test")
@Api(description = "test")
public class TestController {

    @GetMapping(value = "/get",produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation("")
    public ResponseEntity get(){

        return ResponseEntity.ok("ok");
    }
}
