package insw.ssm.QC.controller;


import insw.ssm.QC.dto.BaseLog;
import insw.ssm.QC.dto.PibResLog;
import insw.ssm.QC.service.TblPibResService;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("/v1/PibRes")
public class TblPibResController extends BaseController {

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());
    @Autowired
    TblPibResService tblPibResService;

    @GetMapping(value = "/getByCar", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation("get car")
    ResponseEntity getByCar(BaseLog request) {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.add("Content-Type", "application/vnd.openmlformats-officedocument.spreadsheetml.sheet");
            headers.add("Content-Disposition", "attachment; filename=" + request + ".xlsx");


            return new ResponseEntity<>(tblPibResService.findCar(request), headers, HttpStatus.OK);

        } catch (Exception e) {
            logger.info(e.getMessage());
            return badRequest(e.getMessage());

        }
    }
}
