package insw.ssm.QC.repository;

import insw.ssm.QC.entity.TblDok;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;

public interface TblDokRepository extends CrudRepository<TblDok, Long> {
    @Transactional
    @Query(value = "select * from tx_ssm_dok",nativeQuery = true)
    List<TblDok> findAll();
}
