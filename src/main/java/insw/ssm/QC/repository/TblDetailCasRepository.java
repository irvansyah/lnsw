package insw.ssm.QC.repository;

import insw.ssm.QC.entity.TblDetailCas;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;

public interface TblDetailCasRepository extends CrudRepository<TblDetailCas, Long> {
    @Transactional
    @Query(value = "select * from tx_ssm_detail_cas",nativeQuery = true)
    List<TblDetailCas> findAll();
}
