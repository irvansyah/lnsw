package insw.ssm.QC.repository;

import insw.ssm.QC.entity.TblKontainer;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;

public interface TblKontainerRepository extends CrudRepository<TblKontainer, Long> {

    @Transactional
    @Query(value = "select * from tx_ssm_kontainer",nativeQuery = true)
    List<TblKontainer> findAll();
}
