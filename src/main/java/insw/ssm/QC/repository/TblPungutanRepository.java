package insw.ssm.QC.repository;

import insw.ssm.QC.entity.TblPungutan;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TblPungutanRepository extends CrudRepository <TblPungutan, String> {
    @Query(value = "select * from tx_ssm_pungutan",nativeQuery = true)
    List<TblPungutan> find();
}
