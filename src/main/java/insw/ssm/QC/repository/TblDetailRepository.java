package insw.ssm.QC.repository;

import insw.ssm.QC.entity.TblDetail;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;

public interface TblDetailRepository extends CrudRepository<TblDetail, Long> {
    @Transactional
    @Query(value = "select * from tx_ssm_detail",nativeQuery = true)
    List<TblDetail> findAll();
}
