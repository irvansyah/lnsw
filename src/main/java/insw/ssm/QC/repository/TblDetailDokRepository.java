package insw.ssm.QC.repository;


import insw.ssm.QC.entity.TblDetailDok;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;

public interface TblDetailDokRepository extends CrudRepository<TblDetailDok, Long> {
    @Transactional
    @Query(value = "select * from tx_ssm_detail_dok",nativeQuery = true)
    List<TblDetailDok> findAll();
}
