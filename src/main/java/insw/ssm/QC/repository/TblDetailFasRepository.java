package insw.ssm.QC.repository;

import insw.ssm.QC.entity.TblDetailFas;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TblDetailFasRepository extends CrudRepository <TblDetailFas , Long> {
    @Query(value = "select * from tx_ssm_detail_fas",nativeQuery = true)
    List<TblDetailFas> findAll();
}
