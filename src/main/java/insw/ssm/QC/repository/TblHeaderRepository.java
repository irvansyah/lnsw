package insw.ssm.QC.repository;

import  insw.ssm.QC.entity.TblHeader;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;


import javax.transaction.Transactional;
import java.util.List;

public interface TblHeaderRepository extends CrudRepository<TblHeader,String> {


    @Transactional
    @Query(value = "select * from tx_ssm_header",nativeQuery = true)
    List<TblHeader> findAll();
}
