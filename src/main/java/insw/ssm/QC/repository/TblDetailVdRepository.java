package insw.ssm.QC.repository;

import insw.ssm.QC.entity.TblDetailDok;
import insw.ssm.QC.entity.TblDetailVd;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;

public interface TblDetailVdRepository extends CrudRepository<TblDetailVd, Long> {
    @Transactional
    @Query(value = "select * from tx_ssm_detail_vd",nativeQuery = true)
    List<TblDetailVd> findAll();
}
