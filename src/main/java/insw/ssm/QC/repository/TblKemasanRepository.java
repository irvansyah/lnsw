package insw.ssm.QC.repository;

import insw.ssm.QC.entity.TblKemasan;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TblKemasanRepository extends CrudRepository<TblKemasan, Long> {
    @Query(value = "select * from tx_ssm_kemasan", nativeQuery = true)
    List<TblKemasan> findAll();
}
