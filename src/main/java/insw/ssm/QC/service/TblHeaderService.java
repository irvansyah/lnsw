package insw.ssm.QC.service;


import insw.ssm.QC.dto.TblHeaderDto;
import insw.ssm.QC.dto.TblHeaderLog;
import insw.ssm.QC.entity.TblHeader;
import insw.ssm.QC.repository.TblHeaderRepository;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.*;

@Service
public class TblHeaderService {


    @Autowired
    EntityManager entityManager;
    @Autowired
    TblHeaderRepository tblHeaderRepo;
    public byte[] findAll(TblHeaderLog request) throws Exception {
        Map<String,Object>params=new HashMap<>();
        List<String>headerData=new ArrayList<>();
        headerData.add("CAR");
        headerData.add("No NOMOR_AJU");
        headerData.add("HDR_DATE");
        headerData.add("ORGID");
        headerData.add("TRADER_ID_TYPE");
        headerData.add("TRADER_ID");
        headerData.add("TRADER_NAME");
        headerData.add("TRADER_ADDRESS");
        headerData.add("TRADER_TYPE");
        headerData.add("TRADER_API_TYPE");
        headerData.add("TRADER_API");
        headerData.add("NPWP_PEMUSATAN");
        headerData.add("NAMA_PEMUSATAN");
        headerData.add("ALAMAT_PEMUSATAN");
        headerData.add("SUPPLIER_ID");
        headerData.add("SUPPLIER_NAME");
        headerData.add("SUPPLIER_ADDRESS");
        headerData.add("SUPPLIER_COUNTRY");
        headerData.add("OWNER_ID_TYPE");
        headerData.add("OWNER_ID");
        headerData.add("OWNER_NAME");
        headerData.add("OWNER_ADDRESS");
        headerData.add("SENDER_NAME");
        headerData.add("SENDER_ADDRESS");
        headerData.add("SENDER_COUNTRY");
        headerData.add("PARTNER_ID");
        headerData.add("PPJK_ID_TYPE");
        headerData.add("PPJK_ID");
        headerData.add("PPJK_NAME");
        headerData.add("PPJK_ADDRESS");
        headerData.add("PPJK_NO");
        headerData.add("PPJK_DATE");
        headerData.add("PENJUAL_NAME");
        headerData.add("PENJUAL_ADDRESS");
        headerData.add("PENJUAL_COUNTRY");
        headerData.add("PENJUAL_COUNTRY_DESC");
        headerData.add("NPWP_PUSAT");
        headerData.add("JNSTRX");
        headerData.add("KET_JNSTRX");
        headerData.add("JML_CONTAINER");
        headerData.add("TRANS_TIPE");
        headerData.add("TRANS_NAME");
        headerData.add("TRANS_NO");
        headerData.add("TRANS_FLAG");
        headerData.add("TRANS_ARR_DATE");
        headerData.add("TRANSIT_TIPE");
        headerData.add("TRANSIT_NAME");
        headerData.add("TRANSIT_NO");
        headerData.add("TRANSIT_FLAG");
        headerData.add("CURRENCY_ID");
        headerData.add("CURRENCY_VAL");
        headerData.add("PRICE_CODE");
        headerData.add("BTAMBAHAN");
        headerData.add("DISCOUNT");
        headerData.add("CNF");
        headerData.add("FOB");
        headerData.add("FREIGHT");
        headerData.add("INSURANCE_TYPE");
        headerData.add("INSURANCE");
        headerData.add("CIF");
        headerData.add("CIF_IDR");
        headerData.add("BRUTO");
        headerData.add("NETTO");
        headerData.add("STATUS_VD");
        headerData.add("TOTAL_VD");
        headerData.add("PORT_LOADING");
        headerData.add("PORT_DISCHARGE");
        headerData.add("PORT_TRANSIT");
        headerData.add("PIC_ID");
        headerData.add("PIC_NAME");
        headerData.add("PIC_POSITION");
        headerData.add("PIC_ADDRESS");
        headerData.add("PIC_PHONE");
        headerData.add("PIC_EMAIL");
        headerData.add("CUSTOMS_OFFICE");
        headerData.add("DOK_TUTUP_TYPE");
        headerData.add("DOK_TUTUP_NO");
        headerData.add("DOK_TUTUP_TGL");
        headerData.add("NO_POS");
        headerData.add("SUB_POS");
        headerData.add("SUB_SUB_POS");
        headerData.add("SKEP_FASILITAS");
        headerData.add("DOC_TYPE");
        headerData.add("IMPORT_TYPE");
        headerData.add("PAYEE_TYPE");
        headerData.add("TPS");
        headerData.add("REG_NO");
        headerData.add("REG_DATE");
        headerData.add("STATUS");
        headerData.add("USER_CREATE");
        headerData.add("DATE_CREATE");
        headerData.add("USER_UPDATE");
        headerData.add("LAST_UPDATE");
        headerData.add("NIB");
        headerData.add("FL_MITA");
        headerData.add("FL_EST_JALUR");
        headerData.add("PIC_CITY");

        List<List<Object>>data=new ArrayList<>();
        String sqlQuery="select * from tx_ssm_header";
//        System.out.println(sqlQuery);
        List<String>value=new ArrayList<>();
        List<String>valueCar = new ArrayList<>();
        String pattern="yyyy-MM-dd ";
        SimpleDateFormat simpleDateFormat=new SimpleDateFormat(pattern);
        String dateStart,dateEnd;
        try{
            if(request.getDateStart()!=null&&!request.getDateStart().toString().isEmpty()){
                dateStart=simpleDateFormat.format(request.getDateStart());
                value.add(" HDR_DATE >= :dateStarts");
                params.put("dateStarts",dateStart);
            }
            if(request.getDateEnd()!=null&&!request.getDateEnd().toString().isEmpty()){
                dateEnd=simpleDateFormat.format(request.getDateEnd());
                value.add(" HDR_DATE <= :dateEnds");
                params.put("dateEnds",dateEnd);
            }
            if (request.getCarID()!=null&&!request.getCarID().isEmpty()){
                List<String> listParams = Arrays.asList(request.getCarID().split(","));
                int no = 0;
                for (String param: listParams){
                    valueCar.add("CAR LIKE :CAR"+no);
                    params.put("CAR"+no,"%"+param+"%");
                    no++;
                    System.out.println("'%"+param+"%'");
                }
            }
            if (value.size() > 0||valueCar.size()>0)
                sqlQuery = sqlQuery + " where ";

            if(value.size()>0){
                sqlQuery=sqlQuery+"(";
                for (int i = 0; i < value.size(); i++) {
                    sqlQuery += value.get(i);
                    if (i != value.size() - 1) {
                        sqlQuery += " and ";
                    }
                }
                sqlQuery=sqlQuery+")";
                if(valueCar.size()>0)
                    sqlQuery=sqlQuery+" and ";
            }

            if(valueCar.size()>0){
                sqlQuery=sqlQuery+"(";

                for (int i = 0; i < valueCar.size(); i++) {
                    sqlQuery += valueCar.get(i);
                    if (i != valueCar.size() - 1) {
                        sqlQuery += " or ";
                    }
                }
                sqlQuery=sqlQuery+")";

            }



            sqlQuery+=" order by CAR , HDR_DATE";
//            System.out.println(sqlQuery);
            Query query=entityManager.createNativeQuery(sqlQuery);
            for(String key :params.keySet()){
                query.setParameter(key,params.get(key));
            }
//            List<TblHeader> tblHeaders= new ArrayList<>();
            List<Object[]>rowList=query.getResultList();

            ArrayList<Object>row=null;
//            for (TxSsmHeaderBpom txSsmHeaderBpom:txSsmHeaderBpoms) {
//                TxSsmHeaderBpomDto txSsmHeaderBpomDto = new TxSsmHeaderBpomDto();
//                BeanUtils.copyProperties(txSsmHeaderBpom,txSsmHeaderBpomDto);
//                txSsmHeaderBpomDtos.add(txSsmHeaderBpomDto);
//
//            }
            for (Object[] rowData:rowList){
                if(rowData[0]!=null){
                    row=new ArrayList<>();
                    row.add(rowData[0]);
                    row.add(rowData[1]);
                    row.add(rowData[2]);
                    row.add(rowData[3]);
                    row.add(rowData[4]);
                    row.add(rowData[5]);
                    row.add(rowData[6]);
                    row.add(rowData[7]);
                    row.add(rowData[8]);
                    row.add(rowData[9]);
                    row.add(rowData[10]);
                    row.add(rowData[11]);
                    row.add(rowData[12]);
                    row.add(rowData[13]);
                    row.add(rowData[14]);
                    row.add(rowData[15]);
                    row.add(rowData[16]);
                    row.add(rowData[17]);
                    row.add(rowData[18]);
                    row.add(rowData[19]);
                    row.add(rowData[20]);
                    row.add(rowData[21]);
                    row.add(rowData[22]);
                    row.add(rowData[23]);
                    row.add(rowData[24]);
                    row.add(rowData[25]);
                    row.add(rowData[26]);
                    row.add(rowData[27]);
                    row.add(rowData[28]);
                    row.add(rowData[29]);
                    row.add(rowData[30]);
                    row.add(rowData[31]);
                    row.add(rowData[32]);
                    row.add(rowData[33]);
                    row.add(rowData[34]);
                    row.add(rowData[35]);
                    row.add(rowData[36]);
                    row.add(rowData[37]);
                    row.add(rowData[38]);
                    row.add(rowData[39]);
                    row.add(rowData[40]);
                    row.add(rowData[41]);
                    row.add(rowData[42]);
                    row.add(rowData[43]);
                    row.add(rowData[44]);
                    row.add(rowData[45]);
                    row.add(rowData[46]);
                    row.add(rowData[47]);
                    row.add(rowData[48]);
                    row.add(rowData[49]);
                    row.add(rowData[50]);
                    row.add(rowData[51]);
                    row.add(rowData[52]);
                    row.add(rowData[53]);
                    row.add(rowData[54]);
                    row.add(rowData[55]);
                    row.add(rowData[56]);
                    row.add(rowData[57]);
                    row.add(rowData[58]);
                    row.add(rowData[59]);
                    row.add(rowData[60]);
                    row.add(rowData[61]);
                    row.add(rowData[62]);
                    row.add(rowData[63]);
                    row.add(rowData[64]);
                    row.add(rowData[65]);
                    row.add(rowData[66]);
                    row.add(rowData[67]);
                    row.add(rowData[68]);
                    row.add(rowData[69]);
                    row.add(rowData[70]);
                    row.add(rowData[71]);
                    row.add(rowData[72]);
                    row.add(rowData[73]);
                    row.add(rowData[74]);
                    row.add(rowData[75]);
                    row.add(rowData[76]);
                    row.add(rowData[77]);
                    row.add(rowData[78]);
                    row.add(rowData[79]);
                    row.add(rowData[80]);
                    row.add(rowData[81]);
                    row.add(rowData[82]);
                    row.add(rowData[83]);
                    row.add(rowData[84]);
                    row.add(rowData[85]);
                    row.add(rowData[86]);
                    row.add(rowData[87]);
                    row.add(rowData[88]);
                    row.add(rowData[89]);
                    row.add(rowData[90]);
                    row.add(rowData[91]);
                    row.add(rowData[92]);
                    row.add(rowData[93]);
                    row.add(rowData[94]);
                    row.add(rowData[95]);
                    row.add(rowData[96]);
                }
                data.add(row);
            }
            return generateXLS(headerData,data,"TblPibHeader");
        }catch (Exception e){
            throw new Exception(e.getMessage());
        }
//        List<TxSsmHeaderBpom> txSsmHeaderBpoms = txssmHeaderBpomRepo.findAll();
    }
    public byte[] generateXLS(List<String> headerData, List<List<Object>> data, String name) throws Exception {

        Workbook workbook = new XSSFWorkbook();

        CreationHelper createHelper = workbook.getCreationHelper();

        Sheet sheet = workbook.createSheet(name);
        Row header = sheet.createRow(0);

        int colNo = 0;
        int rowNo = 1;

        CellStyle headerStyle = workbook.createCellStyle();
        headerStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

        headerStyle.setBorderLeft(BorderStyle.THIN);
        headerStyle.setBorderBottom(BorderStyle.THIN);
        headerStyle.setBorderRight(BorderStyle.THIN);
        headerStyle.setBorderTop(BorderStyle.THIN);

        CellStyle dataStyle = workbook.createCellStyle();
        dataStyle.setBorderLeft(BorderStyle.THIN);
        dataStyle.setBorderBottom(BorderStyle.THIN);
        dataStyle.setBorderRight(BorderStyle.THIN);
        dataStyle.setBorderTop(BorderStyle.THIN);

        CellStyle dateStyle = workbook.createCellStyle();
        dateStyle.setDataFormat(createHelper.createDataFormat().getFormat("d/m/yy"));
        dateStyle.setBorderLeft(BorderStyle.THIN);
        dateStyle.setBorderBottom(BorderStyle.THIN);
        dateStyle.setBorderRight(BorderStyle.THIN);
        dateStyle.setBorderTop(BorderStyle.THIN);

        for (String txt : headerData) {

            Cell headerCell = header.createCell(colNo);
            headerCell.setCellValue(txt);
            headerCell.setCellStyle(headerStyle);

            colNo++;
        }
        for (List<Object> row : data) {

            Row dataRow = sheet.createRow(rowNo);
            rowNo++;
            colNo = 0;

            for (Object obj : row) {

                Cell cell = dataRow.createCell(colNo);

                if (obj == null) {

                    cell.setCellValue("");
                    cell.setCellStyle(dataStyle);
                }
                else if (obj instanceof LocalDate){

                    cell.setCellValue(Date.from(((LocalDate) obj).atStartOfDay().toInstant(ZoneOffset.UTC)));
                    cell.setCellStyle(dateStyle);
                }
                else if ((obj instanceof Double) || (obj instanceof Long) || (obj instanceof Integer)){

                    cell.setCellValue(Double.parseDouble(String.valueOf(obj)));
                    cell.setCellStyle(dataStyle);
                }
                else if (obj instanceof Date) {

                    cell.setCellValue(String.valueOf(obj));
                    cell.setCellStyle(dateStyle);
                }
                else {

                    cell.setCellValue(String.valueOf(obj));
                    cell.setCellStyle(dataStyle);
                }

                colNo++;
            }
        }
        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        try {
            workbook.write(bos);
        }
        finally {
            bos.close();
            workbook.close();
        }

        return bos.toByteArray();
    }
    public List<TblHeaderDto> get(){
        List<TblHeader> tblHeaders = tblHeaderRepo.findAll();
        List<TblHeaderDto> tblHeaderDtos =new ArrayList<>();
        for (TblHeader tblHeader  :tblHeaders ) {
            TblHeaderDto tblHeaderDto = new TblHeaderDto();
            BeanUtils.copyProperties(tblHeader,tblHeaderDto);
            tblHeaderDtos.add(tblHeaderDto);
        }
        return tblHeaderDtos;
    }
}