package insw.ssm.QC.service;

import insw.ssm.QC.dto.BaseLog;
import insw.ssm.QC.repository.TblPungutanRepository;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.io.ByteArrayOutputStream;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.*;

@Service
public class TblFasService {

    @Autowired
    EntityManager entityManager;
//    @Autowired
//    TblPungutanRepository tblPungutanRepository;

    public byte[] findCar(BaseLog request) throws Exception {
        Map<String, Object> params = new HashMap<>();
        List<String> headerData = new ArrayList<>();
        headerData.add("CAR");
        headerData.add("serial");
        headerData.add("asuransi");
        headerData.add("bm");
        headerData.add("bmbesartarif");
        headerData.add("bmjenistarif");
        headerData.add("cukaisaldoawal");
        headerData.add("cukaisaldoakhir");
        headerData.add("ppn");
        headerData.add("pphringan");
        headerData.add("pph");
        headerData.add("ppbnmringan");
        headerData.add("ppnbm");
        headerData.add("bmad");
        headerData.add("bmadringan");
        headerData.add("bmads");
        headerData.add("bmtpjenistarif");
        headerData.add("bmtp");
        headerData.add("bmtps");
        headerData.add("bmibesartarif");
        headerData.add("bmi");
        headerData.add("bmis");
        headerData.add("bmpjenistarif");
        headerData.add("bmp");
        headerData.add("bmps");


        List<List<Object>> data = new ArrayList<>();
        String sqlQuery = "select a.CAR," +
                "a.SERIAL ," +
                "b.ASURANSI, " +
                "b.BM ," +
                "b.BM_BESAR_TARIF , " +
                "b.BM_JENIS_TARIF , " +
                "b.CUKAI_SALDO_AWL , " +
                "b.CUKAI_SALDO_AHR ," +
                "b.PPN ," +
                "b.PPH_RINGAN , " +
                "b.PPH , " +
                "b.PPNBM_RINGAN , " +
                "b.PPNBM , " +
                "b.BMAD , " +
                "b.BMAD_RINGAN ," +
                "b.BMADS, " +
                "b.BMTP_JENIS_TARIF , " +
                "b.BMTP , " +
                "b.BMTPS," +
                "b.BMI_BESAR_TARIF ," +
                "b.BMI , " +
                "b.BMIS , " +
                "b.BMP_JENIS_TARIF , " +
                "b.BMP ," +
                "b.BMPS " +
                "from tx_ssm_detail_fas a join tx_ssm_detail b on b.CAR = a.CAR ";
        List<String> valueCar = new ArrayList<>();
        try {
            if (request.getCar() != null && !request.getCar().toString().isEmpty()) {
                List<String> listParams = Arrays.asList(request.getCar().split(","));
                int no = 0;
                for (String param : listParams) {
                    valueCar.add("CAR LIKE :CAR" + no);
                    params.put("CAR" + no, "%" + param + "%");
                    no++;
                    System.out.println("'%" + param + "%'");
                }
                if (valueCar.size() > 0)
                    sqlQuery = sqlQuery + " where ";

                for (int i = 0; i < valueCar.size(); i++) {
                    sqlQuery += valueCar.get(i);
                    if (i != valueCar.size() - 1) {
                        sqlQuery += " and ";
                    }
                }


                sqlQuery += " order by b.CAR ";
                System.out.println(sqlQuery);
                Query query = entityManager.createNativeQuery(sqlQuery);
                for (String key : params.keySet()) {
                    query.setParameter(key, params.get(key));
                }
                List<Object[]> rowList = query.getResultList();
                ArrayList<Object> row = null;

                for (Object[] rowData : rowList) {
                    if (rowData[0] != null) {
                        row = new ArrayList<>();
                        row.add(rowData[0]);
                        row.add(rowData[1]);
                        row.add(rowData[2]);
                        row.add(rowData[3]);
                        row.add(rowData[4]);
                        row.add(rowData[5]);
                        row.add(rowData[6]);
                        row.add(rowData[7]);
                        row.add(rowData[8]);
                        row.add(rowData[9]);
                        row.add(rowData[10]);
                        row.add(rowData[11]);
                        row.add(rowData[12]);
                        row.add(rowData[13]);
                        row.add(rowData[14]);
                        row.add(rowData[15]);
                        row.add(rowData[16]);
                        row.add(rowData[17]);
                        row.add(rowData[18]);
                        row.add(rowData[19]);
                        row.add(rowData[20]);
                        row.add(rowData[21]);
                        row.add(rowData[22]);
                        row.add(rowData[23]);
                        row.add(rowData[24]);
                    }
                    data.add(row);
                }
            }
            return generateXLS(headerData, data, "TblPibdetfas");

        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    } public byte[] generateXLS (List < String > headerData, List < List < Object >> data, String name) throws
            Exception {

        Workbook workbook = new XSSFWorkbook();

        CreationHelper createHelper = workbook.getCreationHelper();

        Sheet sheet = workbook.createSheet(name);
        Row header = sheet.createRow(0);

        int colNo = 0;
        int rowNo = 1;

        CellStyle headerStyle = workbook.createCellStyle();
        headerStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

        headerStyle.setBorderLeft(BorderStyle.THIN);
        headerStyle.setBorderBottom(BorderStyle.THIN);
        headerStyle.setBorderRight(BorderStyle.THIN);
        headerStyle.setBorderTop(BorderStyle.THIN);

        CellStyle dataStyle = workbook.createCellStyle();
        dataStyle.setBorderLeft(BorderStyle.THIN);
        dataStyle.setBorderBottom(BorderStyle.THIN);
        dataStyle.setBorderRight(BorderStyle.THIN);
        dataStyle.setBorderTop(BorderStyle.THIN);

        CellStyle dateStyle = workbook.createCellStyle();
        dateStyle.setDataFormat(createHelper.createDataFormat().getFormat("d/m/yy"));
        dateStyle.setBorderLeft(BorderStyle.THIN);
        dateStyle.setBorderBottom(BorderStyle.THIN);
        dateStyle.setBorderRight(BorderStyle.THIN);
        dateStyle.setBorderTop(BorderStyle.THIN);

        for (String txt : headerData) {

            Cell headerCell = header.createCell(colNo);
            headerCell.setCellValue(txt);
            headerCell.setCellStyle(headerStyle);

            colNo++;
        }
        for (List<Object> row : data) {

            Row dataRow = sheet.createRow(rowNo);
            rowNo++;
            colNo = 0;

            for (Object obj : row) {

                Cell cell = dataRow.createCell(colNo);

                if (obj == null) {

                    cell.setCellValue("");
                    cell.setCellStyle(dataStyle);
                } else if (obj instanceof LocalDate) {

                    cell.setCellValue(Date.from(((LocalDate) obj).atStartOfDay().toInstant(ZoneOffset.UTC)));
                    cell.setCellStyle(dateStyle);
                } else if ((obj instanceof Double) || (obj instanceof Long) || (obj instanceof Integer)) {

                    cell.setCellValue(Double.parseDouble(String.valueOf(obj)));
                    cell.setCellStyle(dataStyle);
                } else if (obj instanceof Date) {

                    cell.setCellValue(String.valueOf(obj));
                    cell.setCellStyle(dateStyle);
                } else {

                    cell.setCellValue(String.valueOf(obj));
                    cell.setCellStyle(dataStyle);
                }

                colNo++;
            }
        }
        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        try {
            workbook.write(bos);
        } finally {
            bos.close();
            workbook.close();
        }

        return bos.toByteArray();

    }
}

