package insw.ssm.QC.service;

import insw.ssm.QC.dto.BaseLog;
import insw.ssm.QC.repository.TblDetailCasRepository;
import insw.ssm.QC.repository.TblDetailVdRepository;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.io.ByteArrayOutputStream;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.*;

@Service
public class TblDetailVdService {
    @Autowired
    EntityManager entityManager;
    @Autowired
    TblDetailVdRepository tblDetailVdRepository;

    public byte[] findCar(BaseLog request) throws Exception {
        Map<String, Object> params = new HashMap<>();
        List<String> headerData = new ArrayList<>();
        headerData.add("ID_DETVD");
        headerData.add("CAR");
        headerData.add("ID_BARANG");
        headerData.add("JNS_TRANS");
        headerData.add("NILAI_DITAMBAH");
        headerData.add("JATUH_TEMPO");

        List<List<Object>> data = new ArrayList<>();
        String sqlQuery = "select * from tx_ssm_detail_vd";
        List<String> valueCar = new ArrayList<>();
        try {
            if (request.getCar() != null && !request.getCar().toString().isEmpty()) {
                List<String> listParams = Arrays.asList(request.getCar().split(","));
                int no = 0;
                for (String param : listParams) {
                    valueCar.add("CAR LIKE :CAR" + no);
                    params.put("CAR" + no, "%" + param + "%");
                    no++;
                    System.out.println("'%" + param + "%'");
                }
                if (valueCar.size() > 0)
                    sqlQuery = sqlQuery + " where ";

                if(valueCar.size()>0){
                    sqlQuery=sqlQuery+"(";

                    for (int i = 0; i < valueCar.size(); i++) {
                        sqlQuery += valueCar.get(i);
                        if (i != valueCar.size() - 1) {
                            sqlQuery += " or ";
                        }
                    }
                    sqlQuery=sqlQuery+")";

                }

                sqlQuery += " order by CAR ";
                Query query = entityManager.createNativeQuery(sqlQuery);
                for (String key : params.keySet()) {
                    query.setParameter(key, params.get(key));
                }
//                List<TblHeader> tblHeader = new ArrayList<>();
                List<Object[]> rowList = query.getResultList();
                ArrayList<Object> row = null;

                for (Object[] rowData : rowList) {
                    if (rowData[0] != null) {
                        row = new ArrayList<>();
                        row.add(rowData[0]);
                        row.add(rowData[1]);
                        row.add(rowData[2]);
                        row.add(rowData[3]);
                        row.add(rowData[4]);
                        row.add(rowData[5]);

                    }
                    data.add(row);
                }
            }
            return generateXLS(headerData, data, "TblPidDtlVd");

        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    public byte[] generateXLS (List < String > headerData, List < List < Object >> data, String name) throws
            Exception {

        Workbook workbook = new XSSFWorkbook();

        CreationHelper createHelper = workbook.getCreationHelper();

        Sheet sheet = workbook.createSheet(name);
        Row header = sheet.createRow(0);

        int colNo = 0;
        int rowNo = 1;

        CellStyle headerStyle = workbook.createCellStyle();
        headerStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

        headerStyle.setBorderLeft(BorderStyle.THIN);
        headerStyle.setBorderBottom(BorderStyle.THIN);
        headerStyle.setBorderRight(BorderStyle.THIN);
        headerStyle.setBorderTop(BorderStyle.THIN);

        CellStyle dataStyle = workbook.createCellStyle();
        dataStyle.setBorderLeft(BorderStyle.THIN);
        dataStyle.setBorderBottom(BorderStyle.THIN);
        dataStyle.setBorderRight(BorderStyle.THIN);
        dataStyle.setBorderTop(BorderStyle.THIN);

        CellStyle dateStyle = workbook.createCellStyle();
        dateStyle.setDataFormat(createHelper.createDataFormat().getFormat("d/m/yy"));
        dateStyle.setBorderLeft(BorderStyle.THIN);
        dateStyle.setBorderBottom(BorderStyle.THIN);
        dateStyle.setBorderRight(BorderStyle.THIN);
        dateStyle.setBorderTop(BorderStyle.THIN);

        for (String txt : headerData) {

            Cell headerCell = header.createCell(colNo);
            headerCell.setCellValue(txt);
            headerCell.setCellStyle(headerStyle);

            colNo++;
        }
        for (List<Object> row : data) {

            Row dataRow = sheet.createRow(rowNo);
            rowNo++;
            colNo = 0;

            for (Object obj : row) {

                Cell cell = dataRow.createCell(colNo);

                if (obj == null) {

                    cell.setCellValue("");
                    cell.setCellStyle(dataStyle);
                } else if (obj instanceof LocalDate) {

                    cell.setCellValue(Date.from(((LocalDate) obj).atStartOfDay().toInstant(ZoneOffset.UTC)));
                    cell.setCellStyle(dateStyle);
                } else if ((obj instanceof Double) || (obj instanceof Long) || (obj instanceof Integer)) {

                    cell.setCellValue(Double.parseDouble(String.valueOf(obj)));
                    cell.setCellStyle(dataStyle);
                } else if (obj instanceof Date) {

                    cell.setCellValue(String.valueOf(obj));
                    cell.setCellStyle(dateStyle);
                } else {

                    cell.setCellValue(String.valueOf(obj));
                    cell.setCellStyle(dataStyle);
                }

                colNo++;
            }
        }
        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        try {
            workbook.write(bos);
        } finally {
            bos.close();
            workbook.close();
        }

        return bos.toByteArray();

    }
}




