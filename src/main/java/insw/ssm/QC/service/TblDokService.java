package insw.ssm.QC.service;

import insw.ssm.QC.dto.BaseLog;
import insw.ssm.QC.dto.TblDokDto;
import insw.ssm.QC.entity.TblDok;
import insw.ssm.QC.repository.TblDokRepository;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.io.ByteArrayOutputStream;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.*;

@Service
public class TblDokService {

    @Autowired
    EntityManager entityManager;
    @Autowired
    TblDokRepository tblDokRepository;

    public List<TblDokDto> get(){
        List<TblDok> tblDoks = tblDokRepository.findAll();
        List<TblDokDto> tblDokDtos = new ArrayList<>();
        for (TblDok tblDok : tblDoks){
            TblDokDto tblDokDto = new TblDokDto();
            BeanUtils.copyProperties(tblDok,tblDokDto);
            tblDokDtos.add(tblDokDto);
        }
        return tblDokDtos;
    }
    public byte [] findCar(BaseLog request) throws Exception {
        Map<String, Object> params = new HashMap<>();
        List<String> headerData = new ArrayList<>();
        headerData.add("DOK_ID");
        headerData.add("CAR");
        headerData.add("SERI");
        headerData.add("DOK_KODE");
        headerData.add("DOK_NO");
        headerData.add("DOK_TGL");
        headerData.add("DOK_PATH");
        headerData.add("DOK_FILENAME");
        headerData.add("DOK_EXT");
        headerData.add("DOK_SIZE");
        headerData.add("DOK_NEGARA");
        headerData.add("LAMPIRAN");
        headerData.add("DOK_GA");
        headerData.add("STATUS_DEL");
        headerData.add("LARTAS_STAT");

        List<List<Object>> data = new ArrayList<>();
        String sqlQuery = "select * from tx_ssm_dok";
        List<String> valueCar = new ArrayList<>();
        try {
            if (request.getCar() != null && !request.getCar().toString().isEmpty()) {
                List<String> listParams = Arrays.asList(request.getCar().split(","));
                int no = 0;
                for (String param : listParams) {
                    valueCar.add("CAR LIKE :CAR" + no);
                    params.put("CAR" + no, "%" + param + "%");
                    no++;
                    System.out.println("'%" + param + "%'");
                }
                if (valueCar.size() > 0)
                    sqlQuery = sqlQuery + " where ";

                for (int i = 0; i < valueCar.size(); i++) {
                    sqlQuery += valueCar.get(i);
                    if (i != valueCar.size() - 1) {
                        sqlQuery += " and ";
                    }
                }
                sqlQuery += " order by CAR ";
                Query query = entityManager.createNativeQuery(sqlQuery);
                for (String key : params.keySet()) {
                    query.setParameter(key, params.get(key));
                }
//
                List<Object[]> rowList = query.getResultList();
                ArrayList<Object> row = null;

                for (Object[] rowData : rowList){
                    if (rowData[0] != null){
                        row = new ArrayList<>();
                        row.add(rowData[0]);
                        row.add(rowData[1]);
                        row.add(rowData[2]);
                        row.add(rowData[3]);
                        row.add(rowData[4]);
                        row.add(rowData[5]);
                        row.add(rowData[6]);
                        row.add(rowData[7]);
                        row.add(rowData[8]);
                        row.add(rowData[9]);
                        row.add(rowData[10]);
                        row.add(rowData[11]);
                        row.add(rowData[12]);
                        row.add(rowData[13]);
                        row.add(rowData[14]);
                    }
                    data.add(row);
                }
            }
            return generateXLS(headerData, data, "tblPidDok");
        }catch (Exception e){
            throw new Exception(e.getMessage());
        }
    }
    public byte [] generateXLS(List < String > headerData, List < List< Object >> data, String name) throws Exception{

        Workbook workbook = new XSSFWorkbook();

        CreationHelper createHelper = workbook.getCreationHelper();

        Sheet sheet = workbook.createSheet(name);
        Row header = sheet.createRow(0);

        int colNo = 0;
        int rowNo = 1;

        CellStyle headerStyle = workbook.createCellStyle();
        headerStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

        headerStyle.setBorderLeft(BorderStyle.THIN);
        headerStyle.setBorderBottom(BorderStyle.THIN);
        headerStyle.setBorderRight(BorderStyle.THIN);
        headerStyle.setBorderTop(BorderStyle.THIN);

        CellStyle dataStyle = workbook.createCellStyle();
        dataStyle.setBorderLeft(BorderStyle.THIN);
        dataStyle.setBorderBottom(BorderStyle.THIN);
        dataStyle.setBorderRight(BorderStyle.THIN);
        dataStyle.setBorderTop(BorderStyle.THIN);

        CellStyle dateStyle = workbook.createCellStyle();
        dateStyle.setDataFormat(createHelper.createDataFormat().getFormat("d/m/yy"));
        dateStyle.setBorderLeft(BorderStyle.THIN);
        dateStyle.setBorderBottom(BorderStyle.THIN);
        dateStyle.setBorderRight(BorderStyle.THIN);
        dateStyle.setBorderTop(BorderStyle.THIN);

        for (String txt : headerData){

            Cell headerCell = header.createCell(colNo);
            headerCell.setCellValue(txt);
            headerCell.setCellStyle(headerStyle);

            colNo++;
        }
        for (List<Object> row : data){

            Row dataRow = sheet.createRow(rowNo);
            rowNo++;
            colNo = 0;

            for (Object obj : row){
                Cell cell = dataRow.createCell(colNo);

                if (obj == null){

                    cell.setCellValue("");
                    cell.setCellStyle(dataStyle);
                }else if (obj instanceof LocalDate){

                    cell.setCellValue(Date.from(((LocalDate)obj).atStartOfDay().toInstant(ZoneOffset.UTC)));
                    cell.setCellStyle(dateStyle);
                }else if ((obj instanceof Double)|| (obj instanceof Long) || (obj instanceof Integer)){

                    cell.setCellValue(Double.parseDouble(String.valueOf(obj)));
                    cell.setCellStyle(dataStyle);
                }else if (obj instanceof Date){

                    cell.setCellValue(String.valueOf(obj));
                    cell.setCellStyle(dateStyle);
                }else {

                    cell.setCellValue(String.valueOf(obj));
                    cell.setCellStyle(dataStyle);
                }
                colNo++;
            }
        }
        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        try {
            workbook.write(bos);
        } finally {
            bos.close();
            workbook.close();
        }

        return bos.toByteArray();

    }

}
