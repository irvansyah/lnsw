package insw.ssm.QC.service;

import insw.ssm.QC.dto.BaseLog;
import insw.ssm.QC.dto.TblDetailDto;
import insw.ssm.QC.entity.TblDetail;
import insw.ssm.QC.repository.TblDetailRepository;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.io.ByteArrayOutputStream;
import java.util.*;

@Service
public class TblDetailService {

    @Autowired
    EntityManager entityManager;
    @Autowired
    TblDetailRepository tblDetailRepository;

    public List<TblDetailDto> get() {
        List<TblDetail> tblDetails = tblDetailRepository.findAll();
        List<TblDetailDto> tblDetailDtos = new ArrayList<>();
        for (TblDetail tblDetail : tblDetails) {
            TblDetailDto tblDetailDto = new TblDetailDto();
            BeanUtils.copyProperties(tblDetail, tblDetailDto);
            tblDetailDtos.add(tblDetailDto);
        }
        return tblDetailDtos;
    }

    public byte[] findCar(BaseLog request) throws Exception {
        Map<String, Object> params = new HashMap<>();
        List<String> headerData = new ArrayList<>();
        headerData.add("DTL_ID");
        headerData.add("CAR NOMOR_AJU");
        headerData.add("SERI");
        headerData.add("ID_BARANG");
        headerData.add("HS_CODE");
        headerData.add("HS_DESC");
        headerData.add("URAIAN_BARANG");
        headerData.add("MERK");
        headerData.add("TIPE");
        headerData.add("SPEK_LAIN");
        headerData.add("SPEK_WAJIB");
        headerData.add("SPEK_BRG");
        headerData.add("NEGARA_ASAL");
        headerData.add("KEMASAN_JML");
        headerData.add("KEMASAN_KODE");
        headerData.add("NETTO");
        headerData.add("TOTAL_DTL");
        headerData.add("FOB");
        headerData.add("BT_DISKON");
        headerData.add("FREIGHT");
        headerData.add("SATUAN_JML");
        headerData.add("SATUAN_KODE");
        headerData.add("SATUAN_HARGA");
        headerData.add("ASURANSI");
        headerData.add("HARGA_CIF");
        headerData.add("CIF");
        headerData.add("BM_JML_SATUAN");
        headerData.add("BM_SATUAN");
        headerData.add("BM_JENIS_TARIF");
        headerData.add("BMAD_JENIS_TARIF");
        headerData.add("BMTP_JENIS_TARIF");
        headerData.add("BMI_JENIS_TARIF");
        headerData.add("BMP_JENIS_TARIF");
        headerData.add("BMK_JENIS_TARIF");
        headerData.add("BM_BESAR_TARIF");
        headerData.add("BMAD_BESAR_TARIF");
        headerData.add("BMTP_BESAR_TARIF");
        headerData.add("BMI_BESAR_TARIF");
        headerData.add("BMP_BESAR_TARIF");
        headerData.add("BMK_BESAR_TARIF");
        headerData.add("BM_PER_TARIF");
        headerData.add("BM_JML_TARIF");
        headerData.add("BM");
        headerData.add("BM_RINGAN");
        headerData.add("BM_RINGAN_PERSEN");
        headerData.add("BMAD");
        headerData.add("BMADS");
        headerData.add("BMAD_RINGAN");
        headerData.add("BMAD_RINGAN_PERSEN");
        headerData.add("BMTP");
        headerData.add("BMTPS");
        headerData.add("BMTP_RINGAN");
        headerData.add("BMTP_RINGAN_PERSEN");
        headerData.add("BMI");
        headerData.add("BMIS");
        headerData.add("BMI_RINGAN");
        headerData.add("BMI_RINGAN_PERSEN");
        headerData.add("BMP");
        headerData.add("BMPS");
        headerData.add("BMP_RINGAN");
        headerData.add("BMP_RINGAN_PERSEN");
        headerData.add("BMK");
        headerData.add("BMK_RINGAN");
        headerData.add("BMK_RINGAN_PERSEN");
        headerData.add("PPN");
        headerData.add("PPN_RINGAN");
        headerData.add("PPN_RINGAN_PERSEN");
        headerData.add("PPNBM");
        headerData.add("PPNBM_RINGAN");
        headerData.add("PPNBM_RINGAN_PERSEN");
        headerData.add("PPH");
        headerData.add("PPH_RINGAN");
        headerData.add("PPH_RINGAN_PERSEN");
        headerData.add("FASILITAS");
        headerData.add("CUKAI_KMDT");
        headerData.add("KMDT_DET");
        headerData.add("CUKAI_TRF");
        headerData.add("CUKAI_BESAR_TRF");
        headerData.add("CUKAI_JML_IMPOR");
        headerData.add("CUKAI_RP");
        headerData.add("CUKAI_TRF_PERSEN");
        headerData.add("CUKAI_TRF_RINGAN");
        headerData.add("CUKAI_TRF_RINGAN_PERSEN");
        headerData.add("CUKAI_MEREK");
        headerData.add("CUKAI_HJE");
        headerData.add("CUKAI_ISI_PER");
        headerData.add("CUKAI_JML_PER");
        headerData.add("CUKAI_SALDO_AWL");
        headerData.add("CUKAI_SALDO_AHR");
        headerData.add("MUTU");
        headerData.add("KONDISI_BRG");
        headerData.add("FL_LARTAS");
        headerData.add("ID_KONT");
        headerData.add("USER_CREATE");
        headerData.add("DATE_CREATE");
        headerData.add("USER_UPDATE");
        headerData.add("LAST_UPDATE");


        List<List<Object>> data = new ArrayList<>();
        String sqlQuery = "select * from tx_ssm_detail";
        List<String> valueCar = new ArrayList<>();
        try {
            if (request.getCar() != null && !request.getCar().toString().isEmpty()) {
                List<String> listParams = Arrays.asList(request.getCar().split(","));
                int no = 0;
                for (String param : listParams) {
                    valueCar.add("CAR LIKE :CAR" + no);
                    params.put("CAR" + no, "%" + param + "%");
                    no++;
                    System.out.println("'%" + param + "%'");
                }
                if (valueCar.size() > 0)
                    sqlQuery = sqlQuery + " where ";

                for (int i = 0; i < valueCar.size(); i++) {
                    sqlQuery += valueCar.get(i);
                    if (i != valueCar.size() - 1) {
                        sqlQuery += " and ";
                    }
                }
                sqlQuery += " order by CAR ";
                Query query = entityManager.createNativeQuery(sqlQuery);
                for (String key : params.keySet()) {
                    query.setParameter(key, params.get(key));
                }
                List<Object[]> rowList = query.getResultList();
                ArrayList<Object> row = null;

                    for (Object[] rowData : rowList) {
                        if (rowData[0] != null) {
                            row = new ArrayList<>();
                            row.add(rowData[0]);
                            row.add(rowData[1]);
                            row.add(rowData[2]);
                            row.add(rowData[3]);
                            row.add(rowData[4]);
                            row.add(rowData[5]);
                            row.add(rowData[6]);
                            row.add(rowData[7]);
                            row.add(rowData[8]);
                            row.add(rowData[9]);
                            row.add(rowData[10]);
                            row.add(rowData[11]);
                            row.add(rowData[12]);
                            row.add(rowData[13]);
                            row.add(rowData[14]);
                            row.add(rowData[15]);
                            row.add(rowData[16]);
                            row.add(rowData[17]);
                            row.add(rowData[18]);
                            row.add(rowData[19]);
                            row.add(rowData[20]);
                            row.add(rowData[21]);
                            row.add(rowData[22]);
                            row.add(rowData[23]);
                            row.add(rowData[24]);
                            row.add(rowData[25]);
                            row.add(rowData[26]);
                            row.add(rowData[27]);
                            row.add(rowData[28]);
                            row.add(rowData[29]);
                            row.add(rowData[30]);
                            row.add(rowData[31]);
                            row.add(rowData[32]);
                            row.add(rowData[33]);
                            row.add(rowData[34]);
                            row.add(rowData[35]);
                            row.add(rowData[36]);
                            row.add(rowData[37]);
                            row.add(rowData[38]);
                            row.add(rowData[39]);
                            row.add(rowData[40]);
                            row.add(rowData[41]);
                            row.add(rowData[42]);
                            row.add(rowData[43]);
                            row.add(rowData[44]);
                            row.add(rowData[45]);
                            row.add(rowData[46]);
                            row.add(rowData[47]);
                            row.add(rowData[48]);
                            row.add(rowData[49]);
                            row.add(rowData[50]);
                            row.add(rowData[51]);
                            row.add(rowData[52]);
                            row.add(rowData[53]);
                            row.add(rowData[54]);
                            row.add(rowData[55]);
                            row.add(rowData[56]);
                            row.add(rowData[57]);
                            row.add(rowData[58]);
                            row.add(rowData[59]);
                            row.add(rowData[60]);
                            row.add(rowData[61]);
                            row.add(rowData[62]);
                            row.add(rowData[63]);
                            row.add(rowData[64]);
                            row.add(rowData[65]);
                            row.add(rowData[66]);
                            row.add(rowData[67]);
                            row.add(rowData[68]);
                            row.add(rowData[69]);
                            row.add(rowData[70]);
                            row.add(rowData[71]);
                            row.add(rowData[72]);
                            row.add(rowData[73]);
                            row.add(rowData[74]);
                            row.add(rowData[75]);
                            row.add(rowData[76]);
                            row.add(rowData[77]);
                            row.add(rowData[78]);
                            row.add(rowData[79]);
                            row.add(rowData[80]);
                            row.add(rowData[81]);
                            row.add(rowData[82]);
                            row.add(rowData[83]);
                            row.add(rowData[84]);
                            row.add(rowData[85]);
                            row.add(rowData[86]);
                            row.add(rowData[87]);
                            row.add(rowData[88]);
                            row.add(rowData[89]);
                            row.add(rowData[90]);
                            row.add(rowData[91]);
                            row.add(rowData[92]);
                            row.add(rowData[93]);
                            row.add(rowData[94]);
                            row.add(rowData[95]);
                            row.add(rowData[96]);
                        }
                        data.add(row);
                    }
                }
                return generateXLS(headerData, data, "tblPibDtl");


        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }
    public byte[] generateXLS(List < String > headerData, List < List < Object >> data, String name)throws Exception {

        Workbook workbook = new XSSFWorkbook();

        CreationHelper createHelper = workbook.getCreationHelper();

        Sheet sheet = workbook.createSheet(name);
        Row header = sheet.createRow(0);

        int colNo = 0;
        int rowNo = 1;

        CellStyle headerStyle = workbook.createCellStyle();
        headerStyle.setFillForegroundColor(IndexedColors.GREY_40_PERCENT.getIndex());
        headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

        headerStyle.setBorderLeft(BorderStyle.THIN);
        headerStyle.setBorderBottom(BorderStyle.THIN);
        headerStyle.setBorderRight(BorderStyle.THIN);
        headerStyle.setBorderTop(BorderStyle.THIN);

        CellStyle dataStyle = workbook.createCellStyle();
        dataStyle.setBorderLeft(BorderStyle.THIN);
        dataStyle.setBorderBottom(BorderStyle.THIN);
        dataStyle.setBorderRight(BorderStyle.THIN);
        dataStyle.setBorderTop(BorderStyle.THIN);
        CellStyle dateStyle = workbook.createCellStyle();
        dateStyle.setDataFormat(createHelper.createDataFormat().getFormat("d/m/yy"));
        dateStyle.setBorderLeft(BorderStyle.THIN);
        dateStyle.setBorderBottom(BorderStyle.THIN);
        dateStyle.setBorderRight(BorderStyle.THIN);
        dateStyle.setBorderTop(BorderStyle.THIN);

        for (String txt : headerData){
            Cell headerCell = header.createCell(colNo);
            headerCell.setCellValue(txt);
            headerCell.setCellStyle(headerStyle);
            colNo++;
        }
        for (List<Object> row : data){

            Row dataRow = sheet.createRow(rowNo);
            rowNo++;
            colNo = 0;

            for (Object obj : row){
                Cell cell = dataRow.createCell(colNo);

                if (obj == null){

                    cell.setCellValue("");
                    cell.setCellStyle(dataStyle);
                } else  if ((obj instanceof  Double)|| (obj instanceof Long)||(obj instanceof Integer)){

                    cell.setCellValue(Double.parseDouble(String.valueOf(obj)));
                    cell.setCellStyle(dataStyle);
                }else  if (obj instanceof  Date){
                    cell.setCellValue(String.valueOf(obj));
                    cell.setCellStyle(dataStyle);
                }else {
                    cell.setCellValue(String.valueOf(obj));
                    cell.setCellStyle(dataStyle);
                }
                colNo++;
            }
        }
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            workbook.write(bos);
        }finally {
            bos.close();
        }
        return bos.toByteArray();
    }
}

