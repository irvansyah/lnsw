package insw.ssm.QC.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "tx_ssm_detail_fas")
public class TblDetailFas {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_FAS")
    private Double idFas;
    @Column(name = "CAR")
    private String car;
    @Column(name = "ID_BARANG")
    private long idBarang;
    @Column(name = "SERIAL")
    private String serial;
    @Column(name = "KDSKEPFAS")
    private String kdKepFas;
    @Column(name = "KDDOKFAS")
    private String kdDokFas;
    @Column(name = "SERI_DOK")
    private Double seriDok;
    @Column(name = "KDGRUPDOK")
    private String kdGrupDok;
}








