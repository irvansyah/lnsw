package insw.ssm.QC.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "tx_ssm_kemasan")
public class TblKemasan {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "KEMASAN_ID")
    private Long kemasanId;
    @Column(name = "CAR")
    private String car;
    @Column(name = "SERI")
    private Double seri;
    @Column(name = "MERK")
    private String merk;
    @Column(name = "JUMLAH")
    private Double jumlah;
    @Column(name = "KEMASAN_KODE")
    private String kemasanKode;
    @Column(name = "BAHAN")
    private String bahan;
    @Column(name = "USER_CREATE")
    private Double userCreate;
    @Column(name = "DATE_CREATE")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd kk:mm:ss",timezone = "Asia/Jakarta")
    private Date dateCreate;
    @Column(name = "USER_UPDATE")
    private Double userUpdate;
    @Column(name = "LAST_UPDATE")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd kk:mm:ss",timezone = "Asia/Jakarta")
    private Date lastUpdate;
}










