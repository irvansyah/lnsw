package insw.ssm.QC.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "tx_ssm_detail")
public class TblDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "DTL_ID")
    private Double dtlId;
    @Column(name = "CAR")
    private String car;
    @Column(name = "SERI")
    private Double seri;
    @Column(name = "ID_BARANG")
    private Double idBarang;
    @Column(name = "HS_CODE")
    private String hsCode;
    @Column(name = "HS_DESC")
    private String hsDesc;
    @Column(name = "URAIAN_BARANG")
    private String uraianBarang;
    @Column(name = "MERK")
    private String merk;
    @Column(name = "TIPE")
    private String tipe;
    @Column(name = "SPEK_LAIN")
    private String spekLain;
    @Column(name = "SPEK_WAJIB")
    private String spekWajib;
    @Column(name = "SPEK_BRG")
    private String spekBrg;
    @Column(name = "NEGARA_ASAL")
    private String negaraAsal;
    @Column(name = "KEMASAN_JML")
    private Double kemasanJml;
    @Column(name = "KEMASAN_KODE")
    private String kemasanKode;
    @Column(name = "NETTO")
    private Double netto;
    @Column(name = "TOTAL_DTL")
    private Double totalDtl;
    @Column(name = "FOB")
    private Double fob;
    @Column(name = "BT_DISKON")
    private Double btDiskon;
    @Column(name = "FREIGHT")
    private Double freight;
    @Column(name = "SATUAN_JML")
    private Double satuanJml;
    @Column(name = "SATUAN_KODE")
    private String satuanKode;
    @Column(name = "SATUAN_HARGA")
    private Double satuanHarga;
    @Column(name = "ASURANSI")
    private Double asuransi;
    @Column(name = "HARGA_CIF")
    private Double hargaCif;
    @Column(name = "CIF")
    private Double cif;
    @Column(name = "BM_JML_SATUAN")
    private Double bmJmlSatuan;
    @Column(name = "BM_SATUAN")
    private String bmsatuan;
    @Column(name = "BM_JENIS_TARIF")
    private String bmJenisTarif;
    @Column(name = "BMAD_JENIS_TARIF")
    private String bmadJenisTarif;
    @Column(name = "BMTP_JENIS_TARIF")
    private String bmtpjenisTarif;
    @Column(name = "BMI_JENIS_TARIF")
    private String bmiJenistarif;
    @Column(name = "BMP_JENIS_TARIF")
    private String bmpJenisTarif;
    @Column(name = "BMK_JENIS_TARIF")
    private String bmkJenistarif;
    @Column(name = "BM_BESAR_TARIF")
    private Double transTipe;
    @Column(name = "BMAD_BESAR_TARIF")
    private Double bmadBesarTarif;
    @Column(name = "BMTP_BESAR_TARIF")
    private Double bmtpBesarTarif;
    @Column(name = "BMI_BESAR_TARIF")
    private Double bmiBesarTarif;
    @Column(name = "BMP_BESAR_TARIF")
    private Double bmpBesarTarif;
    @Column(name = "BMK_BESAR_TARIF")
    private Double bmkBesarTarif;
    @Column(name = "BM_PER_TARIF")
    private Double bmPertarif;
    @Column(name = "BM_JML_TARIF")
    private Double bmJmlTarif;
    @Column(name = "BM")
    private Double bm;
    @Column(name = "BM_RINGAN")
    private String bmRingan;
    @Column(name = "BM_RINGAN_PERSEN")
    private Double bmRinganPersen;
    @Column(name = "BMAD")
    private Double bmad;
    @Column(name = "BMADS")
    private String bmads;
    @Column(name = "BMAD_RINGAN")
    private String bmadRingan;
    @Column(name = "BMAD_RINGAN_PERSEN")
    private Double bmadRinganPersen;
    @Column(name = "BMTP")
    private Double bmtp;
    @Column(name = "BMTPS")
    private String bmtps;
    @Column(name = "BMTP_RINGAN")
    private String bmtpRingan;
    @Column(name = "BMTP_RINGAN_PERSEN")
    private Double bmtpRinganPersen;
    @Column(name = "BMI")
    private Double bmi;
    @Column(name = "BMIS")
    private String bmis;
    @Column(name = "BMI_RINGAN")
    private String bmiRingan;
    @Column(name = "BMI_RINGAN_PERSEN")
    private Double bmiRinganPersen ;
    @Column(name = "BMP")
    private Double bmp;
    @Column(name = "BMPS", length = 50)
    private String bmps;
    @Column(name = "BMP_RINGAN")
    private String bmpRingan;
    @Column(name = "BMP_RINGAN_PERSEN")
    private Double bmpRinganPersen;
    @Column(name = "BMK")
    private Double bmk;
    @Column(name = "BMK_RINGAN")
    private String bmkRingan;
    @Column(name = "BMK_RINGAN_PERSEN")
    private Double bmkRinganPersen;
    @Column(name = "PPN")
    private Double ppn;
    @Column(name = "PPN_RINGAN")
    private String ppnRingan;
    @Column(name = "PPN_RINGAN_PERSEN")
    private Double ppnRinganPersen;
    @Column(name = "PPNBM")
    private Double ppnbm;
    @Column(name = "PPNBM_RINGAN")
    private String ppnbmRingan;
    @Column(name = "PPNBM_RINGAN_PERSEN")
    private Double ppnbmRinganPersen;
    @Column(name = "PPH")
    private Double pph;
    @Column(name = "PPH_RINGAN")
    private String pphRingan;
    @Column(name = "PPH_RINGAN_PERSEN")
    private Double pphRinganPersen;
    @Column(name = "FASILITAS")
    private String fasilitas;
    @Column(name = "CUKAI_KMDT")
    private String cukaiKmdt;
    @Column(name = "KMDT_DET")
    private String kmdtDet;
    @Column(name = "CUKAI_TRF")
    private String cukaiTrf;
    @Column(name = "CUKAI_BESAR_TRF")
    private Double cukaiBesarTrf;
    @Column(name = "CUKAI_JML_IMPOR")
    private Double cukaiJmlImpor;
    @Column(name = "CUKAI_RP")
    private Double cukaiRp;
    @Column(name = "CUKAI_TRF_PERSEN")
    private Double cukaiTrfPersen;
    @Column(name = "CUKAI_TRF_RINGAN")
    private String cukaiTarifRingan;
    @Column(name = "CUKAI_TRF_RINGAN_PERSEN")
    private Double cukaiTarifRinganPersen;
    @Column(name = "CUKAI_MEREK")
    private String cukaiMerk;
    @Column(name = "CUKAI_HJE")
    private Double cukaiHje;
    @Column(name = "CUKAI_ISI_PER")
    private Double cukaiIsiPer;
    @Column(name = "CUKAI_JML_PER")
    private Double cukaiJmlPer;
    @Column(name = "CUKAI_SALDO_AWL")
    private Double cukaiSaldoAwal;
    @Column(name = "CUKAI_SALDO_AHR")
    private Double cukaiSaldoAhr;
    @Column(name = "MUTU")
    private String mutu;
    @Column(name = "KONDISI_BRG")
    private String kondisiBarang;
    @Column(name = "FL_LARTAS")
    private String flLArtas;
    @Column(name = "ID_KONT")
    private Double idKont;
    @Column(name = "USER_CREATE")
    private Double userCreate;
    @Column(name = "DATE_CREATE")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd kk:mm:ss",timezone = "Asia/Jakarta")
    private Date dateCreate;
    @Column(name = "USER_UPDATE")
    private Double userUpdate;
    @Column(name = "LAST_UPDATE")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd kk:mm:ss",timezone = "Asia/Jakarta")
    private Date lastUpdate;

}































































































