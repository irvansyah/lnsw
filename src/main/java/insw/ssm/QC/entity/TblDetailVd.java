package insw.ssm.QC.entity;


import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "tx_ssm_detail_vd")
public class TblDetailVd {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_DETVD")
    private long idDetVd;
    @Column(name = "CAR")
    private String car;
    @Column(name = "ID_BARANG")
    private Double IdBarang;
    @Column(name = "JNS_TRANS")
    private String jnsTrans;
    @Column(name = "NILAI_DITAMBAH")
    private long nilaiDitambah;

    @Column(name = "JATUH_TEMPO")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd kk:mm:ss",timezone = "Asia/Jakarta")
    private Date jatuhTempo;

}
