package insw.ssm.QC.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "tx_ssm_detail_cas")
public class TblDetailCas {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_CAS")
    private Double idCas;
    @Column(name = "CAR")
    private String car;
    @Column(name = "SERIAL")
    private String serial;
    @Column(name = "ID_BARANG")
    private long idBarang;
    @Column(name = "CAS_NUMBER")
    private String casNumber;

}
