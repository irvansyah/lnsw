package insw.ssm.QC.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name ="tx_ssm_header")
public class TblHeader {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "CAR")
    private String car;
    @Column(name = "NOMOR_AJU")
    private String nomorAju;
    @Column(name = "TRADER_ID_TYPE")
    private String traderIdtype;
    @Column(name = "TRADER_ID")
    private String traderId;
    @Column(name = "TRADER_NAME")
    private String traderName;
    @Column(name = "TRADER_ADDRESS")
    private String traderAddres;
    @Column(name = "TRADER_TYPE")
    private String traderType;
    @Column(name = "TRADER_API_TYPE")
    private String traderApitype;
    @Column(name = "TRADER_API")
    private String traderApi;
    @Column(name = "NPWP_PEMUSATAN")
    private String npwpPemusatan;
    @Column(name = "NAMA_PEMUSATAN")
    private String namaPemusatan;
    @Column(name = "ALAMAT_PEMUSATAN")
    private String alamatPemusatan;
    @Column(name = "SUPPLIER_NAME")
    private String supplierName;
    @Column(name = "SUPPLIER_ADDRESS")
    private String supplierAddress;
    @Column(name = "SUPPLIER_COUNTRY")
    private String supplierCountry;
    @Column(name = "OWNER_ID_TYPE")
    private String ownerIdType;
    @Column(name = "OWNER_ID")
    private String ownerId;
    @Column(name = "OWNER_NAME")
    private String ownerName;
    @Column(name = "OWNER_ADDRESS")
    private String ownerAdress;
    @Column(name = "SENDER_NAME")
    private String senderName;
    @Column(name = "SENDER_ADDRESS")
    private String senderAddress;
    @Column(name = "SENDER_COUNTRY")
    private String senderCountry;
    @Column(name = "PPJK_ID_TYPE")
    private String ppjkIdType;
    @Column(name = "PPJK_ID")
    private String ppjkId;
    @Column(name = "PPJK_NAME")
    private String ppjkName;
    @Column(name = "PPJK_ADDRESS")
    private String ppjkAdress;
    @Column(name = "PPJK_NO")
    private String ppjkNo;
    @Column(name = "PENJUAL_NAME")
    private String penjualName;
    @Column(name = "PENJUAL_ADDRESS")
    private String penjualAddress;
    @Column(name = "PENJUAL_COUNTRY")
    private String penjualCountry;
    @Column(name = "PENJUAL_COUNTRY_DESC")
    private String penjualCountryDesc;
    @Column(name = "NPWP_PUSAT")
    private String npwpPusat;
    @Column(name = "JNSTRX")
    private String jnstrx;
    @Column(name = "KET_JNSTRX")
    private String ketJnstrx;
    @Column(name = "TRANS_TIPE")
    private String transTipe;
    @Column(name = "TRANS_NAME")
    private String transName;
    @Column(name = "TRANS_NO")
    private String transNo;
    @Column(name = "TRANS_FLAG")
    private String transFlag;
    @Column(name = "TRANSIT_TIPE")
    private String transitTipe;
    @Column(name = "TRANSIT_NAME")
    private String transiName;
    @Column(name = "TRANSIT_NO")
    private String transitNo;
    @Column(name = "TRANSIT_FLAG")
    private String transitFlag;
    @Column(name = "CURRENCY_ID")
    private String currencyId;
    @Column(name = "PRICE_CODE")
    private String priceCode;
    @Column(name = "INSURANCE_TYPE")
    private String insurancetype;
    @Column(name = "STATUS_VD")
    private String statusVd;
    @Column(name = "PORT_LOADING")
    private String portLoading;
    @Column(name = "PORT_DISCHARGE")
    private String portDischarge;
    @Column(name = "PORT_TRANSIT")
    private String portTransit;
    @Column(name = "PIC_NAME")
    private String picName;
    @Column(name = "PIC_POSITION")
    private String picPosition;
    @Column(name = "PIC_ADDRESS")
    private String picAddress;
    @Column(name = "PIC_PHONE")
    private String picPhone;
    @Column(name = "PIC_EMAIL")
    private String picEmail;
    @Column(name = "CUSTOMS_OFFICE")
    private String customOffice;
    @Column(name = "DOK_TUTUP_TYPE")
    private String dokTutupType;
    @Column(name = "DOK_TUTUP_NO")
    private String dokTutupNo;
    @Column(name = "NO_POS")
    private String noPos;
    @Column(name = "SUB_POS")
    private String subPos;
    @Column(name = "SUB_SUB_POS")
    private String subSubPos;
    @Column(name = "SKEP_FASILITAS")
    private String skepfasilitas;
    @Column(name = "DOC_TYPE")
    private String docType;
    @Column(name = "IMPORT_TYPE")
    private String importType;
    @Column(name = "PAYEE_TYPE")
    private String payeeType;
    @Column(name = "TPS")
    private String tps;
    @Column(name = "REG_NO")
    private String regNo;
    @Column(name = "STATUS")
    private String status;
    @Column(name = "NIB")
    private String nib;
    @Column(name = "FL_MITA")
    private String flMita;
    @Column(name = "FL_EST_JALUR")
    private String flEstjalur;
    @Column(name = "PIC_CITY")
    private String picCity;
    @Column(name = "ORGID")
    private Double orgid;
    @Column(name = "SUPPLIER_ID")
    private Double supplierId;
    @Column(name = "PARTNER_ID")
    private Double partnerId;
    @Column(name = "JML_CONTAINER")
    private Double jmlContainer;
    @Column(name = "TOTAL_VD")
    private Double totalVd;
    @Column(name = "PIC_ID")
    private Double pidId;
    @Column(name = "USER_CREATE")
    private Double userCreate;
    @Column(name = "USER_UPDATE")
    private Double userUpdate;
    @Column(name = "CURRENCY_VAL")
    private Double currencyVal;
    @Column(name = "BTAMBAHAN")
    private Double biayaTambahan;
    @Column(name = "DISCOUNT")
    private Double discount;
    @Column(name = "CNF")
    private Double cnf;
    @Column(name = "FOB")
    private Double fob;
    @Column(name = "FREIGHT")
    private Double freight;
    @Column(name = "INSURANCE")
    private Double insurance;
    @Column(name = "CIF")
    private Double cif;
    @Column(name = "CIF_IDR")
    private Double cifIdr;
    @Column(name = "BRUTO")
    private Double bruto;
    @Column(name = "NETTO")
    private Double netto;

    @Column(name = "HDR_DATE")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd kk:mm:ss",timezone = "Asia/Jakarta")
    private Date hdrDate;
    @Column(name = "PPJK_DATE")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd kk:mm:ss",timezone = "Asia/Jakarta")
    private Date ppjkDate;
    @Column(name = "TRANS_ARR_DATE")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd kk:mm:ss",timezone = "Asia/Jakarta")
    private Date transArrDate;
    @Column(name = "DOK_TUTUP_TGL")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd kk:mm:ss",timezone = "Asia/Jakarta")
    private Date dokTutupTgl;
    @Column(name = "REG_DATE")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd kk:mm:ss",timezone = "Asia/Jakarta")
    private Date regDate;
    @Column(name = "DATE_CREATE")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd kk:mm:ss",timezone = "Asia/Jakarta")
    private Date dateCreate;
    @Column(name = "LAST_UPDATE")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd kk:mm:ss",timezone = "Asia/Jakarta")
    private Date lastUpdate;
}
