package insw.ssm.QC.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "tx_ssm_detail_dok")
public class TblDetailDok {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "DOK_ID")
    private Double dokId;
    @Column(name = "CAR")
    private String car;
    @Column(name = "SERI")
    private Double seri ;
    @Column(name = "SERI_DOK")
    private Double seriDok;
    @Column(name = "DOK_KODE")
    private String dokKode;
    @Column(name = "DOK_NO")
    private String dokNo;
    @Column(name = "DOK_TGL")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd kk:mm:ss",timezone = "Asia/Jakarta")
    private Date dokTgl;
    @Column(name = "DOK_PATH")
    private String dokPath;
    @Column(name = "DOK_FILENAME")
    private String dokFileName;
    @Column(name = "DOK_EXT")
    private String dokExt;
    @Column(name = "DOK_SIZE")
    private Double dokSize;

}
