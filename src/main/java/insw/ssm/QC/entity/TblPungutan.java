package insw.ssm.QC.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "tx_ssm_pungutan")
public class TblPungutan {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "CAR")
    private String car ;
    @Column(name = "KODE_BEBAN")
    private String kodeBeban;
    @Column(name = "KD_FASILITAS")
    private String jdFasilitas;
    @Column(name = "NILAI")
    private Double nilai;

}




