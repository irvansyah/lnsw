package insw.ssm.QC.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "tx_ssm_dok")
public class TblDok {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "DOK_ID")
    private long dokId;
    @Column(name = "CAR")
    private String car;
    @Column(name = "SERI")
    private long seri;
    @Column(name = "DOK_KODE")
    private String dokKode;
    @Column(name = "DOK_NO")
    private String dokNo;

    @Column(name = "DOK_TGL")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd kk:mm:ss",timezone = "Asia/Jakarta")
    private Date dokTanggal;

    @Column(name = "DOK_PATH")
    private String dokPath;
    @Column(name = "DOK_FILENAME")
    private String dokFileName;
    @Column(name = "DOK_EXT")
    private String dokExt;
    @Column(name = "DOK_SIZE")
    private Float dokSize;
    @Column(name = "DOK_NEGARA")
    private String dokNegara;
    @Column(name = "LAMPIRAN")
    private String lampiran;
    @Column(name = "DOK_GA")
    private String dokGa;
    @Column(name = "STATUS_DEL")
    private String statusDel;
    @Column(name = "LARTAS_STAT")
    private String lartasStat;

}

