package insw.ssm.QC.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "tx_ssm_kontainer")
public class TblKontainer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "KONT_ID")
    private long KontId;
    @Column(name = "CAR")
    private String car;
    @Column(name = "SERI")
    private long seri;
    @Column(name = "NOMOR")
    private String nomor;
    @Column(name = "UKURAN")
    private String ukuran ;
    @Column(name = "TIPE")
    private String tipe ;
    @Column(name = "JENIS")
    private String jenis ;
    @Column(name = "SEGEL")
    private String segel ;
    @Column(name = "USER_CREATE")
    private Double userCreate;
    @Column(name = "DATE_CREATE")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd kk:mm:ss",timezone = "Asia/Jakarta")
    private Date dateCreate;
    @Column(name = "USER_UPDATE")
    private Double userUpdate;
    @Column(name = "LASt_UPDATE")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd kk:mm:ss",timezone = "Asia/Jakarta")
    private Date lastUpdate;

}
