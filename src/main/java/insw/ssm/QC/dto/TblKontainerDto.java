package insw.ssm.QC.dto;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data

public class TblKontainerDto {


    private long KontId;
    private String car;
    private long seri;
    private String nomor;
    private String ukuran;
    private String tipe;
    private String jenis;
    private String segel;
    private Double userCreate;
    private Date dateCreate;
    private Double userUpdate;
    private Date lastUpdate;

}
