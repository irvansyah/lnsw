package insw.ssm.QC.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data

public class TblDetailDokDto {

    private Double dokId;
    private String car;
    private Double seri ;
    private Double seriDok;
    private String dokKode;
    private String dokNo;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd kk:mm:ss",timezone = "Asia/Jakarta")
    private Date dokTgl;
    private String dokPath;
    private String dokFileName;
    private String dokExt;
    private Double dokSize;
}
