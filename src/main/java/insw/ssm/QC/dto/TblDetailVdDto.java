package insw.ssm.QC.dto;


import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
public class TblDetailVdDto {


    private long idDetVd;
    private String car;
    private Double IdBarang;
    private String jnsTrans;
    private long nilaiDitambah;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd kk:mm:ss",timezone = "Asia/Jakarta")
    private Date jatuhTempo;

}

