package insw.ssm.QC.dto;

import lombok.Data;

import javax.persistence.*;

@Data

public class TblDetailCasDto {


    private Double idCas;
    private String car;
    private String serial;
    private long idBarang;
    private String casNumber;

}
