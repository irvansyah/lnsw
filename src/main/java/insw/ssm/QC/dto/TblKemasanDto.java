package insw.ssm.QC.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data

public class TblKemasanDto {



    private String car;
    private String merk;
    private float jumlah;
    private String kemasanKode;
    private Long kemasanId;
    private long seri;
    private String bahan;
    private Long userCreate;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd kk:mm:ss",timezone = "Asia/Jakarta")
    private Date dateCreate;
    private Long userUpdate;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd kk:mm:ss",timezone = "Asia/Jakarta")
    private Date lastUpdate;
}










