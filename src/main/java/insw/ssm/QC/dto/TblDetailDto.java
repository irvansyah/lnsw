package insw.ssm.QC.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;


@Data
public class TblDetailDto {


    private Double dtlId;
    private String car;
    private Double seri;
    private Double idBarang;
    private String hsCode;
    private String hsDesc;
    private String uraianBarang;
    private String merk;
    private String tipe;
    private String spekLain;
    private String spekWajib;
    private String spekBrg;
    private String negaraAsal;
    private Double kemasanJml;
    private String kemasanKode;
    private Double netto;
    private Double totalDtl;
    private Double fob;
    private Double btDiskon;
    private Double freight;
    private Double satuanJml;
    private String satuanKode;
    private Double satuanHarga;
    private Double asuransi;
    private Double hargaCif;
    private Double cif;
    private Double bmJmlSatuan;
    private String bmsatuan;
    private String bmJenisTarif;
    private String bmadJenisTarif;
    private String bmtpjenisTarif;
    private String bmiJenistarif;
    private String bmpJenisTarif;
    private String bmkJenistarif;
    private Double transTipe;
    private Double bmadBesarTarif;
    private Double bmtpBesarTarif;
    private Double bmiBesarTarif;
    private Double bmpBesarTarif;
    private Double bmkBesarTarif;
    private Double bmPertarif;
    private Double bmJmlTarif;
    private Double bm;
    private String bmRingan;
    private Double bmRinganPersen;
    private Double bmad;
    private String bmads;
    private String bmadRingan;
    private Double bmadRinganPersen;
    private Double bmtp;
    private String bmtps;
    private String bmtpRingan;
    private Double bmtpRinganPersen;
    private Double bmi;
    private String bmis;
    private String bmiRingan;
    private Double bmiRinganPersen;
    private Double bmp;
    private String bmps;
    private String bmpRingan;
    private Double bmpRinganPersen;
    private Double bmk;
    private String bmkRingan;
    private Double bmkRinganPersen;
    private Double ppn;
    private String ppnRingan;
    private Double ppnRinganPersen;
    private Double ppnbm;
    private String ppnbmRingan;
    private Double ppnbmRinganPersen;
    private Double pph;
    private String pphRingan;
    private Double pphRinganPersen;
    private String fasilitas;
    private String cukaiKmdt;
    private String kmdtDet;
    private String cukaiTrf;
    private Double cukaiBesarTrf;
    private Double cukaiJmlImpor;
    private Double cukaiRp;
    private Double cukaiTrfPersen;
    private String cukaiTarifRingan;
    private Double cukaiTarifRinganPersen;
    private String cukaiMerk;
    private Double cukaiHje;
    private Double cukaiIsiPer;
    private Double cukaiJmlPer;
    private Double cukaiSaldoAwal;
    private Double cukaiSaldoAhr;
    private String mutu;
    private String kondisiBarang;
    private String flLArtas;
    private Double idKont;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd kk:mm:ss",timezone = "Asia/Jakarta")
    private Double userCreate;
    private Date dateCreate;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd kk:mm:ss",timezone = "Asia/Jakarta")
    private Double userUpdate;




}
































































































