package insw.ssm.QC.dto;

import lombok.Data;



@Data
public class TblDetailFasDto {


        private Long idFas;
        private String car;
        private long idBarang;
        private String serial;
        private String kdKepFas;
        private String kdDokFas;
        private Double seriDok;
        private String kdGrupDok;
}








