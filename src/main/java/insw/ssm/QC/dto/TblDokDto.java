package insw.ssm.QC.dto;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data

public class TblDokDto {

    private long dokId;
    private String car;
    private long seri;
    private String dokKode;
    private String dokNo;
    private Date dokTanggal;
    private String dokPath;
    private String dokFileName;
    private String dokExt;
    private Float dokSize;
    private String dokNegara;
    private String lampiran;
    private String dokGa;
    private String statusDel;
    private String lartasStat;

}

