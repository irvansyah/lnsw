package insw.ssm.QC.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;


@Data
@NoArgsConstructor
public class TblHeaderLog {



    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd ",timezone = "Asia/Jakarta")
    private Date dateStart;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd ",timezone = "Asia/Jakarta")
    private Date dateEnd;

    private String carID;


}
