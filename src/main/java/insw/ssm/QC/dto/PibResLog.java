package insw.ssm.QC.dto;

import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
public class PibResLog {

    private String no_aju;
}
