package insw.ssm.QC.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class BaseLog {

    private String car;

}
